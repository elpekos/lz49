# lz49

LZ49 compression utility used during Z88 Operating System (OZ) compilation. LZ49 has been designed and written by Edouard Bergé in 2016.

